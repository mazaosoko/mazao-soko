from io import BytesIO
from PIL import Image
from django.core.files import File


from .models import Cart

class GetTotalAMount(object):

    def __init__(self,buyer = None):
        if buyer:
            self.buyer = buyer
    
    def get_buyer_total_amount(self):
        items = Cart.objects.filter(buyer = self.buyer)
        total = 0
        for item in items:
            total+=item.total_amount
        return total



def compress(image):
    im = Image.open(image)
    rgb_im = im.convert("RGB")
    im_io = BytesIO()
    rgb_im.save(im_io,"JPEG",quality=60)
    new_image = File(im_io,name=image.name)
    return new_image

        