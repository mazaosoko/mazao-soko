from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import MpesaPayment,Order,Cart

@receiver(post_save,sender = MpesaPayment)
def add_order_items(sender, instance, created, **kwargs):
    client = instance.client
    items_to_order = Cart.objects.filter(buyer=client)
    for item in items_to_order:
        Order.objects.create(payment = instance,product_name = item.product_name,total_amount = item.total_amount)
    



