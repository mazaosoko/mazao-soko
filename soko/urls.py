from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('new-product/', views.newProduct, name='newProduct'),
    path('about/', views.about, name="about"),
    path("product/<int:id>/",views.fetch_product,name="fetch_product"),
    path("fetch-updates/",views.fetch_updates,name="fetch_updates"),
    path("cart/",views.view_cart,name="view_cart"),
    path("add-to-cart/<int:id>/",views.add_to_cart,name="add_to_cart"),
    path("verify-quantity/",views.verify_quantity,name="verify_quantity"),
    path("remove-from-cart/<int:id>/",views.remove_from_cart,name="remove_from_cart"),
    path("update-cart/",views.update_cart,name="update_cart"),
    path("checkout/",views.checkout,name="checkout"),
    path("payment-redirect/",views.payment_redirect,name="payment_redirect"),
    path("clear-cart/",views.clear_cart,name="clear_cart"),
    path("lipa/",views.lipa_na_mpesa_online,name="lipa_na_mpesa"),
    path("call-back/",views.call_back,name="call_back"),
    path("order-history/",views.order_history,name="order_history"),
    path("fetch-order/<int:id>/",views.fetch_order_data,name="fetch_order"),
    path("error/",views.error,name="some_error"),
    path("success/",views.success,name="success")
]
