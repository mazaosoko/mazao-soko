from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from pyuploadcare.dj.models import ImageField

from io import BytesIO
from PIL import Image
from django.core.files import File



class Products(models.Model):
    QUANTITY_CHOICES = (
        ("Bags@90kg", "BAG(s)-90kg"),
        ("Bags@70kg", "BAG(s)-70kg"),
        ("Bags@50kg", "BAG(s)-50kg"),
        ("Nets","Net(s)-13kg"),
        ("Bunches", "Bunches"),
        ("Crates", "Crate(s)"),
        ("Trays","Tray(s)"),
    )
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    product_name = models.CharField(max_length=20)
    product_description = models.TextField()
    quantity = models.PositiveIntegerField()
    measurement_unit = models.CharField(choices=QUANTITY_CHOICES, max_length=9)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    discount_price = models.FloatField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    current_location = models.CharField(max_length = 200,null=True)
    image1 = ImageField(blank=False,manual_crop="") 
    image2 = ImageField(blank=True,manual_crop="") 
    image3 = ImageField(blank=True,manual_crop="") 


    class Meta:
        verbose_name_plural = "Products"
        ordering = ['-timestamp']
        db_table = "products"


    def __str__(self):
        return str(self.owner)


    # def save(self,*args,**kwargs):
    #     if self.image1:
    #         new_image = compress(self.image1)
    #         self.image1 = new_image

    #     if self.image2:
    #         new_image = compress(self.image2)
    #         self.image2 = new_image


    #     if self.image3:
    #         new_image = compress(self.image3)
    #         self.image3 = new_image

            
    #     super().save(*args,**kwargs)



class Cart(models.Model):
    src_id = models.PositiveIntegerField()
    buyer = models.ForeignKey(User,on_delete=models.CASCADE)
    product_name = models.CharField(max_length=100)
    amount = models.PositiveIntegerField()
    quantity = models.PositiveIntegerField(default=1)
    total_amount = models.PositiveIntegerField(default=1)
    timestamp = models.DateTimeField(default=timezone.now)



    def __str__(self):
        return str(self.buyer)
    



class MpesaCallBack(models.Model):
    payment_number = models.CharField(max_length=20,null=True)
    result_code = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.payment_number

    class Meta:
        verbose_name = 'Mpesa Call Back'
        verbose_name_plural = 'Mpesa Call Backs'



class MpesaPayment(models.Model):
    client = models.ForeignKey(User,on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length = 10)
    total_amount = models.DecimalField(max_digits=10, decimal_places=2)
    timestamp = models.DateTimeField(default = timezone.now)


    class Meta:
        verbose_name = 'Mpesa Payment'
        verbose_name_plural = 'Mpesa Payments'


    def __str__(self):
        return self.first_name



class Order(models.Model):
    payment = models.ForeignKey(MpesaPayment,on_delete=models.CASCADE)
    product_name = models.CharField(max_length = 100)
    total_amount = models.PositiveIntegerField()


    def __str__(self):
        return str(self.payment)

class ShippingDetails(models.Model):
    name = models.CharField(max_length=150)
    address = models.CharField(max_length=200)
    building = models.CharField(max_length=200)
    charges = models.PositiveIntegerField(default=100)

    def __str__(self):
        return str(self.name)

    class Meta:
        db_table = "shipping"
