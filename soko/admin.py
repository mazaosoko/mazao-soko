from django.contrib import admin
from .models import Products,MpesaPayment,Cart,MpesaCallBack,Order, ShippingDetails

admin.site.register(Products)
admin.site.register(MpesaPayment)
admin.site.register(Cart)
admin.site.register(MpesaCallBack)
admin.site.register(Order)
admin.site.register(ShippingDetails)


