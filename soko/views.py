import json,uuid,requests

from django.http import JsonResponse,HttpResponse
from django.shortcuts import render, redirect,reverse
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.views.decorators.csrf import csrf_exempt

from .models import Products,MpesaPayment,Cart,MpesaCallBack,Order, ShippingDetails,User
from .forms import AddProductForm,CheckOutForm, ShippingDetailsForm
from .utility import GetTotalAMount
from accounts.models import Profile

from .mpesa_credentials import MpesaAccessToken, LipanaMpesaPpassword


def index(request):
    products = Products.objects.filter(quantity__gte = 1)
    params = {
        "products":products
    }
    return render(request, 'shop/index.html', params)



def newProduct(request):
    user = request.user
    title = "Post Product"
    if request.method == 'POST':
        form = AddProductForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.owner = request.user
            instance.save()
            return redirect('/')
    else:
        form = AddProductForm(auto_id=False)
    return render(request, 'shop/newProduct.html', {"form": form})




def about(request):
    return render(request, 'shop/about.html', {'page_title': 'About'})



def fetch_product(request,id):
    p = Products.objects.get(id = id)
    return render(request,"shop/product.html",{"product":p})



def fetch_updates(request):
    products = Products.objects.filter(quantity__gte = 1).order_by("-timestamp")
    data = serializers.serialize("json",products)
    return JsonResponse(data=data,safe=False)




@login_required
def view_cart(request):
    items = Cart.objects.filter(buyer = request.user)
    total = GetTotalAMount(buyer = request.user).get_buyer_total_amount()
    return render(request,"shop/cart.html",{"items":items,"total":total})




def add_to_cart(request,id):
    _,price = request.POST.get("price").split(" ") 
    total_amount = round((float(price)*int(request.POST.get("quantity"))),2)
    p = Products.objects.get(id = id)
    c = Cart(src_id = id,product_name = p.product_name,amount=float(price),quantity = int(request.POST.get("quantity")),total_amount = total_amount)
    purchase_quantity = int(request.POST.get("quantity"))
    rem_amount = int(p.quantity) - int(purchase_quantity)
    Products.objects.filter(id = id).update(quantity = rem_amount)
    c.buyer = request.user
    c.save()
    return JsonResponse({"added":True})





def verify_quantity(request):
    raw_string = str(request.body,"utf-8")
    id = raw_string.split("=")[1]
    return JsonResponse({"quantity":Products.objects.get(id=id).quantity})



def remove_from_cart(request,id):
    to_delete = Cart.objects.get(id=id)
    src_id = to_delete.src_id
    qtty_cart = to_delete.quantity
    main_qtty = Products.objects.get(id=src_id).quantity
    q = qtty_cart+main_qtty
    Products.objects.filter(id = src_id).update(quantity = q)
    to_delete.delete()
    return redirect("view_cart")



def update_cart(request):
    if not request.user.is_anonymous:
        count = Cart.objects.filter(buyer = request.user).count()
    else:
        count = 0
    return JsonResponse({"count":count})



def payment_redirect(request):
    import time
    time.sleep(30)
    try:
        result_code = MpesaCallBack.objects.get(payment_number = Profile.objects.get(user = request.user).phone_number).result_code
    except Exception as e:
        MpesaPayment.objects.filter(client=request.user)[0].delete()
        return redirect("some_error")
    else:

        MpesaCallBack.objects.get(payment_number = Profile.objects.get(user = request.user).phone_number).delete()

        if result_code == 0:

            items = Cart.objects.filter(buyer=request.user)
    
            if items.exists():
                items._raw_delete(items.db)

            return redirect("success")
        
        MpesaPayment.objects.filter(client=request.user)[0].delete()
        return redirect("some_error")
    



def checkout(request):
    total_amount = GetTotalAMount(buyer = request.user).get_buyer_total_amount()
    form = CheckOutForm(initial={"total_amount":total_amount})
    return render(request,"shop/checkout.html",{"form":form})



def clear_cart(request):   
    return JsonResponse({"deleted":True})





def lipa_na_mpesa_online(request):
    data = request.GET
    total_amount = int(float(data["total_amount"]))
    list_num = list(data["phone_number"])
    new_num = list_num[1:]
    s = ""
    for n in new_num:
        s+=str(n)
    num = "254"+s

    access_token = MpesaAccessToken.validated_mpesa_access_token
    api_url = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest"
    call_back_url = request.build_absolute_uri(reverse("call_back"))
    headers = {"Authorization": "Bearer %s" % access_token}
    stk_request = {
        "BusinessShortCode": LipanaMpesaPpassword.Business_short_code,
        "Password": LipanaMpesaPpassword.decode_password,
        "Timestamp": LipanaMpesaPpassword.lipa_time,
        "TransactionType": "CustomerPayBillOnline",
        "Amount": total_amount,
        "PartyA": num,  # replace with your phone number to get stk push
        "PartyB": LipanaMpesaPpassword.Business_short_code,
        "PhoneNumber": num,  # replace with your phone number to get stk push
        "CallBackURL": call_back_url,
        "AccountReference": "MazaoSoko",
        "TransactionDesc": "Testing stk push"
    }
    response = requests.post(api_url, json=stk_request, headers=headers)
    res_data = json.loads(response.text)
    try:
        status = res_data["ResponseCode"]
    except Exception as e:
        pass
    else:
        p = MpesaPayment(client = request.user,first_name = data["first_name"],middle_name = data["middle_name"],last_name = data["last_name"],phone_number = data["phone_number"],total_amount = float(data["total_amount"]))
        p.save()
    return JsonResponse(json.dumps({"Done":True}),safe=False)




@csrf_exempt
def call_back(request):
    raw_data = request.body
    data = json.loads(raw_data)
    code = data["Body"]["stkCallback"]["ResultCode"]
    try:
        payment_number = data["Body"]["stkCallback"]["CallbackMetadata"]["Item"][4]["Value"]
    except Exception as e:
        MpesaCallBack.objects.create(payment_number = -1,result_code = code)
        return HttpResponse(status=200)
    else:
        num_list = list(str(payment_number))
        new_list = num_list[3:]
        new_list.insert(0,0)
        num = ""
        for i in new_list:
            num+=str(i)

        MpesaCallBack.objects.create(payment_number = num,result_code = code)
        
        return HttpResponse(status=200)



def order_history(request):
    order_payments = MpesaPayment.objects.filter(client = request.user).order_by("-timestamp")
    return render(request, "shop/order-history.html",{"order_payments":order_payments})



def fetch_order_data(request,id):
    items = Order.objects.filter(payment = MpesaPayment.objects.get(id = id))
    data = serializers.serialize("json",items)
    return JsonResponse(data = data,safe=False)



def success(request):
    return render(request,"shop/success.html")

def error(request):
    return render(request,"shop/error.html")




