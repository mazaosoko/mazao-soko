from django import forms

from pyuploadcare.dj.forms import ImageField

from .models import Products,MpesaPayment,ShippingDetails

class AddProductForm(forms.ModelForm):

    image1 = ImageField(label="")
    image2 = ImageField(label="")
    image3 = ImageField(label="")

    
    class Meta:
        model = Products
        fields = ['product_name','quantity','measurement_unit','price','product_description','current_location','image1','image2','image3'] #
        widgets = {
            "product_name":forms.TextInput(attrs={"placeholder":"eg Irish Potatoes(Waruu)"}),
            "product_description":forms.Textarea(attrs={"placeholder":"Provide more measurement_unit,quantity, quality and price information about your product to the clients."}),
            "quantity":forms.TextInput(attrs={"placeholder":"eg 10"}),
            "measurement_unit":forms.Select(),
            "price":forms.TextInput(attrs={"placeholder":"eg 1000 per each measurement_unit"}),
            # "discount_price":forms.TextInput(attrs={"placeholder":"Previous selling price"}),
            "current_location":forms.TextInput(attrs={"placeholder":"eg Nyahururu(Suera)"}),
        }



class CheckOutForm(forms.ModelForm):
    class Meta:
        model = MpesaPayment
        exclude = ("client","timestamp")
        widgets= {
            "phone_number":forms.TextInput(attrs={"placeholder":"0712345678"})
        }

    def __init__(self,*args,**kwargs):
        super(CheckOutForm,self).__init__(*args,**kwargs)
        self.fields["total_amount"].widget.attrs["readonly"] = True

class ShippingDetailsForm(forms.ModelForm):
    class Meta:
        model = ShippingDetails
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"placeholder":"e.g John doe"}),
            "address": forms.TextInput(attrs={"placeholder":"4th street Kwame Nkurumah Rd"}),
            "building": forms.TextInput(attrs={"placeholder":"a specific location and building"}),
        }

        def __init__(self,*args,**kwargs):
            super(ShippingDetailsForm,self).__init__(*args,**kwargs)
            self.fields["charges"].widgets.attrs["readonly"]=True
