# MazaoSoko
An e-commerce website for agricultural produce.

### Our Mission
- To help farmers get market for their produce at competitive prices as well as provide consumers with quality farm products in a reliable, timely and efficient way.

### Our Vision
-  To be the leading agribusiness  e-commerce  solutions provider in Africa.

<hr>

## Setup and installations
- You must have python installed in your machine. If not, visit the [official Python website](http://python.org/) to download the latest version for your OS.

-  You must have MySQL installed in your machine. If not, visit the [MySQL website](https://www.mysql.com/downloads/) to download the latest version for your OS.

- Go to the project's root directory and proceed as indicated below.

1. __Install__ and __activate__ your __virtual environment__. Click [this link](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/) for more information.

2. Install dependencies by running ```pip install -r requirements.txt``` in your terminal.

3. Open *MySQL workbench* and run the scripts in **db_setup.sql** line by line.

4. Make migrations by running ```python manage.py makemigrations``` and ```python manage.py migrate``` respectively in your terminal.

5. Run the development server by entering ```python manage.py runserver``` in the terminal.  Go to ```localhost:8000``` in your browser to visit the site's homepage.

6. Explore the different pages through the links in the navigation bar (navbar) 😀

7. Click the **Sell** link in the navbar. You will be redirected to the register page. Create an account and update your profile after logging in.  Note the changes in your navbar. 

8. Close the development server by hitting ```Ctrl + C``` in the terminal

#### Creating an admin user

-  Use the ```python manage.py createsuperuser``` command to create an admin user in your terminal. 

- Go to ```localhost:8000/admin``` in your browser and log in as an administrator to view the site as an admin.

#### Important notes
- Create **Python environment variables** for the email backend and M-pesa consumer key and secret to match the variables given in *settings.py* and *mpesa_credentials.py* files. Click the links below to view tutorials on how to do that for:
    - [Windows](https://www.youtube.com/watch?v=IolxqkL7cD8)
    - [Linux or Mac](https://www.youtube.com/watch?v=5iWhQWVXosU&t=79s)

- Add your virtual environment to the gitignore file.
- Work on your branch!