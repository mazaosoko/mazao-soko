import uuid,json
from django.http import JsonResponse,HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.decorators import login_required
from django.core import serializers,mail
from django.template import loader

from .forms import RegisterForm,CreateProfileForm,ViewUpdateProfileForm,EmailForm,ResetPasswordForm
from .models import Profile
from soko.models import Cart,Products
from django.contrib import messages

User = get_user_model()

def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password1"]
            form.save()
            user = authenticate(username=username,password=password)

            if user:
                login(request,user=user)
                if request.session.get("my_id"):
                    try:
                        cart_items = Cart.objects.filter(customer_id = uuid.UUID(request.session["my_id"]))
                    except:
                        pass
                    else:
                        for item in cart_items:
                            item.buyer = request.user
                            item.save()
                return redirect("create_profile")

    else:
        form = RegisterForm()
    return render(request, "accounts/register.html", {"form": form})


def create_profile(request):
    if request.method == 'POST':
        form = CreateProfileForm(request.POST)
        if form.is_valid() :
            data = form.cleaned_data
            Profile.objects.filter(user = request.user).update(
                username = data["username"],
                about = data["about"],
                phone_number = data["phone_number"]
            )


            return redirect('index')

    else:
        form = CreateProfileForm(instance=request.user)
    return render(request, 'accounts/create_profile.html', {"form": form })


def fetch_profile(request,username):
    url = Profile.objects.get(user = User.objects.get(username = username)).prof_image
    name = username
    return JsonResponse(data = {"name":name,"url":url})


def update_profile(request):
    instance = Profile.objects.get(user = request.user)
    if request.method == "POST":
        form = ViewUpdateProfileForm(request.POST,instance=instance)
        if form.is_valid():
            form.save()
            return redirect("index")

    else:
        form = ViewUpdateProfileForm(instance = instance)
    return render(request,"accounts/update_profile.html",{"form":form})



@login_required
def view_profile(request):
    return render(request, "accounts/view_profile.html")


def reset_password(request):
    form = EmailForm()
    return render(request,"accounts/password_reset.html",{"form":form})


def request_password_reset(request):
    email = request.GET["email"]
    try:
        user_id = User.objects.get(email = email).id
    except:
        pass
    else:
        password_reset_link = request.build_absolute_uri("/")+"account/new-password/"+str(user_id)+"/"
        subject = "Password Reset Link"
        message = ""
        html_mail = loader.render_to_string("accounts/password_reset_mail.html",{
            "link":password_reset_link
        })
        mail.send_mail(subject,message,"mazaosoko@gmail.com",[email,],fail_silently=True,html_message=html_mail)
        return JsonResponse({})


def new_password(request,user_id):
    form = ResetPasswordForm()
    return render(request,"accounts/password_reset_form.html",{"form":form})



def validate_email(request):
    email = request.GET["email"]
    for user in User.objects.all():
        if user.email == email:
            return JsonResponse(data=json.dumps({"exists":True}),safe=False)
    return JsonResponse(data=json.dumps({"exists":False}),safe=False)


def set_new_password(request):
    new_password = request.GET["new_password"]
    user_id = request.GET["user_id"]
    user = User.objects.get(id = user_id)
    user.set_password(new_password)
    user.save()
    return JsonResponse(data=json.dumps({"changed":True}),safe=False)
        
