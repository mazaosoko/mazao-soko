from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path('register/', views.register, name="register"),
    path('login/', auth_views.LoginView.as_view(template_name="accounts/login.html"), name="login"),
    path('logout/', auth_views.LogoutView.as_view(), name="logout"),
    path('create_profile/', views.create_profile, name="create_profile"),
    path('update_profile/', views.update_profile, name="update_profile"),
    path('view_profile/', views.view_profile, name="view_profile"),
    # path("view-seller-profile/<int:id>/",views.view_seller_profile,name="seller_profile"),
    path("fetch-profile/<str:username>/", views.fetch_profile,name="fetch-profile"),
    path('password-reset/',views.reset_password, name="password_reset"),
    path("request-password-reset/",views.request_password_reset,name="request_password_reset"),
    path("new-password/<int:user_id>/",views.new_password,name="new_password"),
    path("validate-email/",views.validate_email,name="validate_email"),
    path("set-new-password/",views.set_new_password,name="set_new_password"),
    # path("email-sent/",views.email_sent,name="email_sent"),
    # path('password-reset/done',
    #     auth_views.PasswordResetDoneView.as_view(template_name="accounts/password_reset_done.html"),
    #         name="password_reset_done"),
    # path('password-reset-confirm/<uidb64>/<token>/', 
    #     auth_views.PasswordResetConfirmView.as_view(template_name="accounts/password_reset_confirm.html"), 
    #         name="password_reset_confirm"),
    # path('password-reset-complete/',
    #     auth_views.PasswordResetCompleteView.as_view(template_name="accounts/password_reset_complete.html"),
    #     name="password_reset_complete") 
]