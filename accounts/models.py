from django.db import models
from django.contrib.auth.models import User

from pyuploadcare.dj.models import ImageField

from soko.utility import compress


# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    username = models.CharField(max_length = 100,blank=True,null=True)
    prof_image = ImageField(blank=True,manual_crop="")
    about = models.TextField(max_length=255, blank=True)
    phone_number = models.CharField(max_length=200)
    join_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.user.username} Profile'


    # def save(self,*args,**kwargs):
    #     if self.image.name != "default.svg":
    #         new_image = compress(self.image)
    #         self.image = new_image
            
    #     super().save(*args,**kwargs)