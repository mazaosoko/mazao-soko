from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

from pyuploadcare.dj.forms import ImageField


from .models import Profile

class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")
    
    def __init__(self,*args,**kwargs):
        super(RegisterForm,self).__init__(*args,**kwargs)

        for key in self.fields.keys():
            self.fields[key].help_text = ""

class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("username", "email")
    
    def __init__(self,*args,**kwargs):
        super(UserUpdateForm,self).__init__(*args,**kwargs)

        for key in self.fields.keys():
            self.fields[key].help_text = ""


class CreateProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ("user","prof_image")
        widgets = {
            "phone_number":forms.TextInput(attrs={"placeholder":"0712345678"})
        }

    def __init__(self,*args,**kwargs):
        super(CreateProfileForm,self).__init__(*args,**kwargs)
        for field in self.fields.keys():
            self.fields[field].help_text = ""

class ProfileUpdateForm(forms.ModelForm):
    prof_image = ImageField(label="")
    class Meta:
        model = Profile
        exclude = ("user",)


class ViewUpdateProfileForm(forms.ModelForm):
    prof_image = ImageField(label="")
    class Meta:
        model = Profile
        exclude = ("user",)
        widgets = {
            "phone_number":forms.TextInput(attrs={"placeholder":"0712345678"})
        }


class EmailForm(forms.Form):
    email = forms.EmailField()



class ResetPasswordForm(forms.Form):
    new_password = forms.CharField(max_length=32,widget=forms.PasswordInput)
    confirm_password = forms.CharField(max_length=32,widget=forms.PasswordInput)

